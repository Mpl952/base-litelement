import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

    static get properties() {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        }
    }

    constructor() {
        super();
        this.resetForm();
    }

    render() {
        return html`
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
          <div>
            <form>
              <div class="form-group">
                <label>Nombre completo</label>
                <input type="text" class="form-control" placeholder="Nombre completo"
                  @input="${this.updateName}"
                  .value="${this.person.name}"
                  ?disabled="${this.editingPerson}"/>
              </div>
              <div class="form-group">
                <label>Perfil</label>
                <textarea class="form-control" placeholder="Perfil" rows="5"
                  @input="${this.updateProfile}" .value="${this.person.profile}"></textarea>
              </div>
              <div class="form-group">
                <label>Años en la empresa</label>
                <input type="text" class="form-control" placeholder="Años en la empresa"
                  @input="${this.updateYears}" .value="${this.person.years}"/>
              </div>
              
              <div>
                <input type="radio" id="chico" name="sexo" value="chico"
                  @click="${this.updatePhoto}"
                  ?checked="${this.person.photo && this.person.photo.src && this.person.photo.src.endsWith('chico.png')}">
                <label for="chico">Hombre</label>
              </div>
              <div>
                <input type="radio" id="chica" name="sexo" value="chica"
                  @click="${this.updatePhoto}"
                  ?checked="${this.person.photo && this.person.photo.src && this.person.photo.src.endsWith('chica.png')}">
                <label for="chica">Mujer</label>
              </div>
              <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
              <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
            </form>
          </div>
        `;
    }

    updateName(ev) {
      console.log('updateName');
      console.log('actualizando propiedad name con valor '+ ev.target.value);
      this.person.name = ev.target.value;
    }

    updateProfile(ev) {
      console.log('updateName');
      console.log('actualizando propiedad name con valor '+ ev.target.value);
      this.person.profile = ev.target.value;
    }

    updateYears(ev) {
      console.log('updateName');
      console.log('actualizando propiedad name con valor '+ ev.target.value);
      this.person.years = ev.target.value;
    }

    updatePhoto(ev) {
      console.log('updatePhoto');
      console.log('actualizando propiedad name con valor '+ ev.target.value);
      const src = ev.target.value === 'chico'
        ? "./img/chico.png"
        : "./img/chica.png";
      this.person.photo = { src, alt: this.person.name};
    }

    goBack(ev) {
      console.log('goBack');
      ev.preventDefault();
      this.resetForm();
      this.dispatchEvent(new CustomEvent('persona-form-close', {}));
    }

    storePerson(ev) {
      console.log('storePerson');
      ev.preventDefault();

      console.log(this.person);

      this.dispatchEvent(new CustomEvent('persona-form-store', {
        detail: {
          person: this.person,
          editingPerson: this.editingPerson
        }
      }));
    }

    resetForm() {
      this.person = {
        name: '',
        profile: '',
        years: '',
        photo: {
          src: '',
          alt: ''
        }
      };
      this.editingPerson = false;
    }
}

customElements.define('persona-form', PersonaForm);