import { LitElement, html } from 'lit-element';

class TestApi extends LitElement{

  static get properties() {
    return {
      movies: {type: Array}
    }
  }

  constructor() {
    super();
    this.movies = [];
    this.getMovies();
  }

  render() {
    return html`
        <h3>Test API</h3>
        ${this.movies.map(
          movie => html`
            <div>La película ${movie.title} la dirigió ${movie.director}</div>
          `
        )}
    `;
  }

  getMovies(){
    console.log('getMovies');

    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
      if (xhr.status === 200) {
        console.log('petición completada correctamente');
        // debugger;
        let APIResponse = JSON.parse(xhr.responseText);
        console.log(APIResponse.results);
        this.movies = APIResponse.results;
      }
    }

    xhr.open('GET', 'https://swapi.dev/api/films/');
    xhr.send()

    console.log('fin getMovies');
  }

}

customElements.define('test-api', TestApi)