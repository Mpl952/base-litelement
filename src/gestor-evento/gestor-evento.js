import { LitElement, html } from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

class GestorEvento extends LitElement{

    render() {
        return html`
            <h2>Gestor Evento</h2>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receptor"></receptor-evento>
        `;
    }

    processEvent(ev) {
      console.log('process event');
      console.log(ev);

      this.shadowRoot.getElementById('receptor').course = ev.detail.course;
      this.shadowRoot.getElementById('receptor').year = ev.detail.year;
    }
}

customElements.define('gestor-evento', GestorEvento)