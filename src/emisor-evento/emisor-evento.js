import { LitElement, html } from 'lit-element';

class EmisorEvento extends LitElement{

    render() {
        return html`
            <h3>Emisor evento</h3>
            <button @click="${this.sendEvent}">No pulsar</button>
        `;
    }

    sendEvent(ev) {
      console.log('send event');
      console.log(ev);
      this.dispatchEvent(new CustomEvent('test-event', {
        detail: {
          course: 'TechU',
          year: 2021
        }
      }));
    }
}

customElements.define('emisor-evento', EmisorEvento)