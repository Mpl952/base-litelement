import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: { type: String },
            yearsInCompany: { type: Number },
            personInfo: { type: String}
        }
    }

    constructor() {
        super();
        this.name = 'Juanito';
        this.yearsInCompany = '2';
    }

    updated(changedProperties) {
        console.log('Updated');
        console.log(changedProperties);
        // changedProperties.forEach((oldValue, propName) => {
        //     console.log('Propiedad: ', propName, '- antiguo valor:', oldValue);
        // });

        changedProperties.forEach((value, key) => {
            console.log('Propiedad: ', key, '- antiguo valor:', value, '- nuevo valor: ', this[key]);
        });

        if (changedProperties.has('yearsInCompany')){
            this.updatePersonInfo();
        }

    }
    
    updatePersonInfo() {
        const years = this.yearsInCompany || 0;
        console.log(years, 'years')
        this.personInfo = years >= 7
            ? 'lead'
            : years >= 5
            ? 'senior'
            : years >= 3
            ? 'team'
            : 'junior';
        console.log(this.personInfo, 'personInfo')
    }

    render() {
        return html`
            <div>
                <label>Nombre completo</label>
                <input type='text' id='fname' name='fname' value='${this.name}' @input="${this.updateName}" />
                <br />
                <label>Años en la empresa</label>
                <input type='text' name='yearsInCompany' value='${this.yearsInCompany}' @input="${this.updateYears}" />
                <br />
                <label>Person info: </label>
                <input type='text' name='personInfo' value='${this.personInfo}' disabled/>
            </div>
        `;
    }

    updateName(ev) {
        console.log('updateName');
        this.name=ev.target.value;
    }

    updateYears(ev) {
        console.log('updateYears');
        this.yearsInCompany=ev.target.value;
    }
}

customElements.define('ficha-persona', FichaPersona);