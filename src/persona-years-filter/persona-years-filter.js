import { LitElement, html } from 'lit-element';

class PersonaYearsFilter extends LitElement {

    static get properties() {
        return {
          maxYearRange: {type: Number}
        }
    }

    constructor() {
        super();
        this.maxYearRange = 50;
    }

    
    render() {
        return html`
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
          <div>
            <label>Años en empresa</label>
            <div class="d-flex justify-content-between">
              <span class="mr-2">0</span>
              <input type="range" min="0" max="${this.maxYearRange}" value="${this.maxYearRange}"
                @input="${this.filterPeopleByYears}">
              <span class="ml-2">${this.maxYearRange}</span>
            </div>
          </div>
        `;
    }

    filterPeopleByYears(ev) {
      console.log('filterPeopleByYears: ', ev.target.value);
      this.dispatchEvent(new CustomEvent('filter-people-by-year', {
        detail: { years: ev.target.value }
      }));
    }
}

customElements.define('persona-years-filter', PersonaYearsFilter);