import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        }
    }

    constructor() {
        super();
    }

    
    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar 
                    @new-person="${this.newPerson}"
                    @filter-people-by-year="${this.filterPeople}" class="col-2"></persona-sidebar>
                <persona-main @updated-people="${this.updatedPeople}" class="col-10"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
        `;
    }

    newPerson(e) {
        console.log('newPerson en persona-app');
        this.shadowRoot.querySelector('persona-main').showPersonForm = true;
        this.shadowRoot.querySelector('persona-main').person = {};
    }

    updated(changedProperties) {
        if (changedProperties.has('people')) {
            console.log('updated en persona-app');
            this.shadowRoot.querySelector('persona-stats').people = this.people;
            this.shadowRoot.querySelector('persona-sidebar').maxYearRange = Math.max(...this.people.map(person => person.years));
        }
    }

    updatedPeople(ev) {
        console.log('updatedPeople en persona-app');
        this.people = ev.detail.people;
    }

    updatedPeopleStats(ev){
        console.log('updatedPeopleStats');
        console.log('stats: ', ev.detail.peopleStats)
        this.shadowRoot.querySelector('persona-sidebar').peopleStats = ev.detail.peopleStats;
        this.shadowRoot.querySelector('persona-main').maxYearsInCompany = ev.detail.peopleStats.maxYearsInCompany;

    }

    filterPeople(ev) {
        console.log('filterPeople en persona-app');
        this.shadowRoot.querySelector('persona-main').maxYearsInCompany = ev.detail.years;
    }
}

customElements.define('persona-app', PersonaApp);