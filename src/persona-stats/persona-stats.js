import { LitElement, html } from 'lit-element';

class PersonaStats extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();
        this.people = [];
    }

    updated(changedProperties) {
      console.log('updated en persona-stats');
      if (changedProperties.has('people')) {
        console.log('detectado cambio en people en persona-stats');

        this.dispatchEvent(
          new CustomEvent(
            'updated-people-stats',
            {
              detail: {
                peopleStats: this.gatherPeopleArrayInfo(this.people)
              }
            }
          )
        );
      }
    }

    gatherPeopleArrayInfo(people) {
      console.log('gatherPeopleArrayInfo');
      return {
        numberOfPeople: people.length,
        maxYearsInCompany: Math.max(...people.map(person => person.years))
      };
    }

}

customElements.define('persona-stats', PersonaStats);