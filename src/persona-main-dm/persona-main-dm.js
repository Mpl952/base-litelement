import { LitElement, html } from 'lit-element';

class PersonaMainDM extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();
        this.people = [
          { name: 'Juan', years: 4, photo: { src: "./img/chico.png", alt: "Juan" }, profile: 'zxfdvsdr ergxdfvbe 5gfb' },
          { name: 'María', years: 6, photo: { src: "./img/chica.png", alt: "María" }, profile: 'dg dfb4bf  5gzxdfv xdfv zxcv dvfd vfs vzdfv zfb' },
          { name: 'Santiago', years: 8, photo: { src: "./img/chico.png", alt: "Santiago"}, profile: 'dtgc ghn. 9 u vbn  cfgbncf 5gfb' },
          { name: 'Jimena', years: 2, photo: { src: "./img/chica.png", alt: "Jimena" }, profile: 'jnzdsfv zxvfklkn frtg 5gfb' },
          { name: 'David', years: 11, photo: { src: "./img/chico.png", alt: "David" }, profile: 'zxfdvsdr ergxdfvbe 5gfb' }
      ];
    }

    updated(changedProperties) {
      console.log('updated en persona-main-dm');
      if (changedProperties.has('people')) {
        console.log('detectado cambio en people en persona-main-dm');

        this.dispatchEvent(
          new CustomEvent(
            'updated-people',
            {
              detail: {
                people: this.people
              }
            }
          )
        );
      }
    }

}

customElements.define('persona-main-dm', PersonaMainDM);