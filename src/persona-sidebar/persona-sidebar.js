import { LitElement, html } from 'lit-element';

import '../persona-years-filter/persona-years-filter.js';

class PersonaSidebar extends LitElement {

    static get properties() {
        return {
            peopleStats: {type: Object},
            maxYearRange: {type: Number}
        }
    }

    constructor() {
        super();
        this.peopleStats = {};
        this.maxYearRange = 20;
    }

    
    render() {
        return html`
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
          <aside>
            <section>
              <div>
                <p>Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas</p>
              </div>
              <div class="mt-5">
                <button class="w-100 btn bg-success" style="font-size: 50px"
                @click="${this.newPerson}"><strong>+</strong></button>
              </div>
              <div class="mt-5">
                <persona-years-filter @filter-people-by-year="${this.filterPeopleByYears}"
                  .maxYearRange="${this.peopleStats.maxYearsInCompany}"></persona-years-filter>
              </div>
            </section>
          </aside>  
        `;
    }

    updated(changedProperties) {
      if (changedProperties.has('maxYearRange')) {
          console.log('updated en persona-sidebar');
          this.shadowRoot.querySelector('persona-years-filter').maxYearRange = this.maxYearRange;
      }
    }

    newPerson(e){
      console.log('newPerson en persona-sidebar');
      console.log('se va a crear una persona');

      this.dispatchEvent(new CustomEvent('new-person', {}));
    }

    filterPeopleByYears(ev) {
      console.log('filterPeopleByYears en persona-sidebar: ', ev.detail);
      this.dispatchEvent(new CustomEvent('filter-people-by-year', {
        detail: ev.detail
      }));
    }
}

customElements.define('persona-sidebar', PersonaSidebar);