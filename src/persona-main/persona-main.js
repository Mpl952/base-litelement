import { LitElement, html } from 'lit-element';

import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-main-dm/persona-main-dm.js';

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: { type: Array },
            showPersonForm: {type: Boolean},
            maxYearsInCompany: { type: Number }
        }
    }

    constructor() {
        super();
        this.people = [];
        this.showPersonForm = false;
    }

    
    render() {
        const peopleFiltered = this.maxYearsInCompany !== undefined 
            ? this.people.filter(person => Number(person.years) <= Number(this.maxYearsInCompany))
            : [...this.people];
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
          <h2 class="text-center">Personas</h2>
          <div class="row" id="peopleList">
            ${peopleFiltered.map(
                person => html`
                <div class="col-2">
                    <persona-ficha-listado 
                        fname="${person.name}" 
                        fyears="${person.years}" 
                        profile="${person.profile}"
                        .photo="${person.photo}"
                        @delete-person="${this.deletePerson}"
                        @more-info="${this.moreInfo}">
                    </persona-ficha-listado>
                </div>
                `
            )}
          </div>
          <div class="row">
              <persona-form id="personForm" 
                @persona-form-close="${this.personFormClose}"
                @persona-form-store="${this.storePerson}" class="d-none"></persona-form>
          </div>
          <persona-main-dm @updated-people="${this.updatedPeople}"></persona-main-dm>
        `;
    }

    updatedPeople(ev) {
        console.log('updatedPeople en persona-main');
        this.people = ev.detail.people;
    }

    updated(changedProperties) {
        console.log('updated');
        if(changedProperties.has('showPersonForm')) {
            console.log('ha cambiado showPersonForm en persona-main');
            console.log(this.showPersonForm);
            this.showPersonForm 
                ? this.showPersonFormData() 
                : this.showPersonList();
        }
        if(changedProperties.has('people')) {
            console.log('ha cambiado people en persona-main');
            this.dispatchEvent(
                new CustomEvent(
                    'updated-people',
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            )
        }

    }

    personFormClose() {
      this.showPersonForm = false;
    }

    storePerson(ev) {
      console.log('storePerson en main');
      console.log(ev.detail.person);

      if(ev.detail.editingPerson) {
        const index = this.people.findIndex(person => person.name === ev.detail.person.name);
        console.log("editing en persona-main", index);
        // if (index > 0){
        //     this.people[index] = {
        //         ...e.detail.person
        //     };
        // }
        this.people = this.people
            .map(
                person => person.name === ev.detail.person.name 
                    ? ev.detail.person
                    : person
            )
      } else {
        console.log("creating en persona-main");
        // this.people.push(ev.detail.person);
        this.people = [
            ...this.people,
            ev.detail.person
        ]
      }
      this.showPersonForm = false;
    }

    showPersonList(){
      console.log('showPersonList');
      this.shadowRoot.getElementById('personForm').classList.add('d-none');
      this.shadowRoot.getElementById('peopleList').classList.remove('d-none');
    }

    showPersonFormData(){
        console.log('showPersonForm');
        this.shadowRoot.getElementById('personForm').classList.remove('d-none');
        this.shadowRoot.getElementById('peopleList').classList.add('d-none');
    }

    deletePerson(e) {
        console.log('deletePerson', e.detail.name);
        this.people = this.people.filter(person => person.name !== e.detail.name);
    }

    moreInfo(e) {
        console.log('moreInfo', e.detail.name);
        const selectedPerson = this.people.find(person => person.name === e.detail.name);
        console.log(selectedPerson);
        this.shadowRoot.getElementById('personForm').person = { ...selectedPerson };
        this.shadowRoot.getElementById('personForm').editingPerson = true;
        this.showPersonForm = true;
    }
}

customElements.define('persona-main', PersonaMain);