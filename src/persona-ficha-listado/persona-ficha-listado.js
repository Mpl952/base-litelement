import { LitElement, html } from 'lit-element';

class PersonaFichaListado extends LitElement {

    static get properties() {
        return {
            fname: {type: String},
            fyears: {type: Number},
            photo: {type: Object},
            profile: {type: String}
        }
    }

    constructor() {
        super();
    }

    
    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <div class="card h-100">
            <img class="card-img-top" src="${this.photo.src}" height="200" width="100" alt="${this.photo.alt}">
            <div class="card-body">
                <h5 class="card-title">${this.fname}</h5>
                <p class="card-text">${this.profile}</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">${this.fyears} años en la empresa</li>
                </ul>
            </div>
            <div class="card-footer">
                <button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>X</strong></button>
                <button @click="${this.moreInfo}" class="btn btn-primary col-5"><strong>Info</strong></button>
            </div>
        </div>
        `;
    }

    deletePerson(e) {
        console.log('deletePerson');
        console.log('se va a borrar la persona con nombre: ', this.fname);

        this.dispatchEvent(new CustomEvent('delete-person', {detail: {name: this.fname}}))
    }

    moreInfo(ev) {
        console.log('moreInfo');
        this.dispatchEvent(new CustomEvent('more-info', {detail: {name: this.fname}}))
    }
}

customElements.define('persona-ficha-listado', PersonaFichaListado);